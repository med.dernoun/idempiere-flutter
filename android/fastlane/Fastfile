# This file contains the fastlane.tools configuration
# You can find the documentation at https://docs.fastlane.tools
#
# For a list of all available actions, check out
#
#     https://docs.fastlane.tools/actions
#
# For a list of all available plugins, check out
#
#     https://docs.fastlane.tools/plugins/available-plugins
#

# Uncomment the line if you want fastlane to automatically update itself
# update_fastlane

default_platform(:android)

platform :android do
  desc "Runs all the tests"
  lane :test do
    gradle(task: "test")
  end

  desc "Submit a new beta build to Google Play"
  lane :beta do
    #sh "fvm flutter clean"
    #sh "fvm flutter build appbundle -v --no-deferred-components"
    sh "./flutter_build.sh --clean"
    upload_to_play_store(
      track: 'beta',
      aab: '../build/app/outputs/bundle/release/app-release.aab',
      json_key_data: ENV['PLAY_STORE_CONFIG_JSON'],
      )
  end

  desc "Promote beta track to prod"
  lane :promote_to_production do
    versions = google_play_track_version_codes(track: "beta")
    version_code = versions[0]
    upload_to_play_store(
      skip_upload_changelogs: true,
      skip_upload_metadata: true,
      skip_upload_images: true,
      skip_upload_screenshots: true,
      track: 'beta',
      track_promote_to: 'production',
      json_key_data: ENV['PLAY_STORE_CONFIG_JSON'],
      version_code: version_code
      )
  end

  desc "Submit a new production build to Google Play"
  lane :production do
    sh "flutter build appbundle -v --no-deferred-components"
    upload_to_play_store(
      track: 'production',
      aab: '../build/app/outputs/bundle/release/app-release.aab',
      json_key_data: ENV['PLAY_STORE_CONFIG_JSON'],
    )
  end
end