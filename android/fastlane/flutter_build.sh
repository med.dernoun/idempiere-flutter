#!/bin/bash
export FLUTTER_HOME="~/fvm/versions/2.0.6"
export PATH=$FLUTTER_HOME/bin:$PATH
cd ../../
if [ "$1" == "--clean" ]
then
   echo "Running clean..."
   flutter clean
else
   echo "Skipping clean..."
fi
if [ "$1" == "--apk" ]
then
   echo "Building APK..."
   flutter build apk --release
else
   echo "Building AAB..."
   flutter build appbundle --release
fi